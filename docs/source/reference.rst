``gff2bed`` reference
=====================

.. autofunction:: gff2bed.parse

.. autofunction:: gff2bed.convert

